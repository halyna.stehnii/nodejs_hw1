// requires...
const fs = require('fs');
const path = require('path');

// constants...

function createFile (req, res, next) {
  try {
    if(!req.body.filename){
      res.status(400).send({
        'message': 'Please specify \'filename\' parameter'
      })
    }

    if(!req.body.content && req.body.content !== ''){
      res.status(400).send({
        'message': 'Please specify \'content\' parameter'
      })
    }

    const allowedExt = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    const ext = req.body.filename ? path.extname(req.body.filename) : undefined;
    const content = req.body.content === '' ? '' : req.body.content;

    if(!ext){
      res.status(400).send({
        'message': 'No extension'
      })
    }

    if(!allowedExt.includes(ext.toString())){
      res.status(400).send({
        'message': 'This extension not allowed'
      })
    }

    fs.writeFileSync(path.join('files', req.body.filename), content)
  
    res.status(200).send({
      "message": "File created successfully"
    });
  } catch (err) {
    next(err)
  }
}

function getFiles (req, res, next) {
  try {
    const files = fs.readdirSync('files');
    res.status(200).send({
      "message": "Success",
      "files": files});
  } catch(err){
    next(err)
  } 
}

const getFile = (req, res, next) => {
  try {
    const filename = req.url.replace('/', '');
    const data = fs.readFileSync(path.join('files', filename), {
      encoding: 'utf-8',
      flag: 'r'
    })
    const { birthtime } = fs.statSync(path.join('files', filename));

    res.status(200).send({
      'message': 'Success',
      'filename': filename,
      'content': data,
      'extension': path.extname(filename).replace('.', ''),
      'uploadDate': birthtime
    });
  } catch(err){
    res.status(400).send({
      'message': 'No file with \'notes.txt\' filename found'
    })
  }
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
